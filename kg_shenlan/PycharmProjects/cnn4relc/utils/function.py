import numpy as np
import codecs
import json
import re

def norm2one(vec):
    root_sum_square = np.sqrt(np.sum(np.square(vec)))
    return vec / root_sum_square


def build_pretrain_embedding(alphabet, embed_dict, embed_dim, norm=True):
    scale = np.sqrt(3.0 / embed_dim)
    pretrain_emb = np.empty([alphabet.size(), embed_dim])
    perfect_match = 0
    case_match = 0
    not_match = 0
    for word, index in alphabet.iteritems():
        if word in embed_dict:
            if norm:
                pretrain_emb[index, :] = norm2one(embed_dict[word])
            else:
                pretrain_emb[index, :] = embed_dict[word]
            perfect_match += 1
        elif word.lower() in embed_dict:
            if norm:
                pretrain_emb[index, :] = norm2one(embed_dict[word.lower()])
            else:
                pretrain_emb[index, :] = embed_dict[word.lower()]
            case_match += 1
        else:
            pretrain_emb[index, :] = np.random.uniform(-scale, scale, [1, embed_dim])
            not_match += 1
    pretrained_size = len(embed_dict)
    print("Embedding:\n     pretrain word: %s, prefect match: %s, case_match: %s, oov: %s, oov_ratio: %s" % (pretrained_size, perfect_match, case_match, not_match, (not_match + 0.) / alphabet.size()))
    return pretrain_emb


def read_instance(file, word_alph, posi_alph, rel_alph, ent_alph, MAX_SENTENCE_LENGTH):
    file = codecs.open(file)
    line = file.readlines()
    id = []
    for i in range(int(len(line)/4)):
        sent = line[4 * i].rstrip().split("\t")[1]
        clean_sent = re.sub("<e1>|</e1>|<e2>|</e2>", "", sent)
        clean_sent = re.sub("[^\w\d\s]", " ", clean_sent).split()
        if len(clean_sent) > MAX_SENTENCE_LENGTH:
            pass
        else:
            word_id = [word_alph.get_index(ele.lower()) for ele in clean_sent]
            entity1 = re.findall("<e1>.*?</e1>", sent)
            entity2 = re.findall("<e2>.*?</e2>", sent)
            entity1 = re.sub("<e1>|</e1>", "", entity1[0])
            entity1 = re.sub("[^\w\d\s]", " ", entity1)#干净的entity string
            entity2 = re.sub("<e2>|</e2>", "", entity2[0])
            entity2 = re.sub("[^\w\d\s]", " ", entity2)#干净的entity string
            entity1_id = ent_alph.get_index(entity1.lower())
            entity2_id = ent_alph.get_index(entity2.lower())

            ent1_len = len(entity1.split())
            ent2_len = len(entity2.split())
            seq1 = re.sub("<e1>.*?</e1>", "#entity1#", sent)
            seq1 = re.sub("<e2>|</e2>", "", seq1)
            seq1 = re.sub(r'[^\w\d\s#]', ' ', seq1).split()
            ent1_pos = seq1.index("#entity1#")
            seq2 = re.sub("<e2>.*?</e2>", "#entity2#", sent)
            seq2 = re.sub("<e1>|</e1>", "", seq2)
            seq2 = re.sub(r'[^\w\d\s#]', ' ', seq2).split()
            ent2_pos = seq2.index("#entity2#")
            position1 = []
            for j in range(len(seq1)):
                if j != ent1_pos:
                    position1.append(j - ent1_pos)
                else:
                    position1 += [0] * ent1_len
            position1_id = [posi_alph.get_index(ele) for ele in position1]#获得每个position1的embedding
            position2 = []
            for j in range(len(seq2)):
                if j != ent2_pos:
                    position2.append(j - ent2_pos)
                else:
                    position2 += [0] * ent2_len
            position2_id = [posi_alph.get_index(ele) for ele in position2]#获得每个position1的embedding
            assert len(position1) == len(position2) == len(clean_sent)

            label = line[4 * i + 1].rstrip()
            rel_vector = rel_alph.get_index(label)
            id.append([word_id, position1_id, position2_id, rel_vector, entity1_id, entity2_id])#前面三个（word_id, position1_id, position2_id）都是list是一个句子中每个词都有，后面三个是值
    return id
