from statistics import mean


def marco_value(predict, gold, excluded_label, label_size):
    TP = [0] * label_size
    TP_FP = [0] * label_size
    TP_FN = [0] * label_size
    for i in range(len(predict)):
        if predict[i] == gold[i]:
            TP[predict[i]] += 1
            TP_FP[predict[i]] += 1
            TP_FN[predict[i]] += 1
        else:
            TP_FP[predict[i]] += 1
            TP_FN[gold[i]] += 1
    p = []
    r = []
    f1 = []
    for i in range(len(TP)):
        if i != excluded_label:
            if TP_FP[i] != 0:
                p.append(TP[i]/TP_FP[i])
            else:
                p.append(0)
            if TP_FN[i] != 0:
                r.append(TP[i]/TP_FN[i])
            else:
                r.append(0)
            if p[i]+r[i] != 0:
                f1.append((2*p[i]*r[i])/(p[i]+r[i]))
            else:
                f1.append(0)
        else:
            p.append(0)
            r.append(0)
            f1.append(0)
    return sum(p)/(label_size-1), sum(r)/(label_size-1), sum(f1)/(label_size-1)
