import sys
from utils.function import *
import re
import codecs
from utils.alphabet import Alphabet
import random
from tqdm import tqdm


START = "</s>"
UNKNOWN = "</unk>"
PADDING = "</pad>"
NULLKEY = "-null-"


class Data:
    def __init__(self):
        self.MAX_SENTENCE_LENGTH = 200
        self.norm_embed = True
        self.word_alphabet = Alphabet('word')
        self.rel_label_alphabet = Alphabet("relation label", unkflag=False, padflag=False)
        self.position_alphabet = Alphabet("position")
        self.entity_alphabet = Alphabet("entity", unkflag=False, padflag=False)

        self.word_embed_dim = -1
        self.position_embed_dim = -1
        self.embed_dict = dict()
        self.embed_alphabet = dict()

        self.train_ids = []
        self.dev_ids = []
        self.test_ids = []
        self.entity_alphabet_size = 0
        self.rel_label_alphabet_size = 0
        self.word_alphabet_size = 0
        self.position_alphabet_size = 0

    def show_data_summary(self):
        print("DATA SUMMARY START:")
        print("     MAX SENTENCE LENGTH: %s" % (self.MAX_SENTENCE_LENGTH))
        print("     Word  alphabet size: %s" % (self.word_alphabet_size))
        print("  relation alphabet size: %s" % (self.rel_label_alphabet_size))
        print("  position alphabet size: %s" % (self.position_alphabet_size))
        print("    entity alphabet size: %s" % (self.entity_alphabet_size))
        print("                                       ")
        print("---------Embedding information---------")
        print("     Word embedding size: %s" % (self.word_embed_dim))
        print(" Position embedding size: %s" % (self.position_embed_dim))
        print("    Norm word embeddings: %s" % (self.norm_embed))
        print("                                       ")
        print("---------Instance information---------")
        print("   Num of Train instance: %s" % (len(self.train_ids)))
        print("     Num of Dev instance: %s" % (len(self.dev_ids)))
        print("    Num of Test instance: %s" % (len(self.test_ids)))
        sys.stdout.flush()

    def build_alphabet(self, file):
        file = [codecs.open(ele) for ele in file]
        for ele in file:
            line = ele.readlines()
            for i in range(int(len(line)/4)):
                inline = line[4 * i]
                inline = inline.rstrip()
                inline = inline.split("\t")[1]
                inline1 = re.sub("<e1>|</e2>|<e2>|</e1>", "", inline)#替换掉<e1>等
                inline1 = re.sub(r'[^\w\d\s]', ' ', inline1)#对于一个句子只留下字母，数字和空格，目的是去除,.等
                inline1 = inline1.split()
                for ele in inline1:
                    if ele != "": #and ele.lower() in self.embed_alphabet
                        self.word_alphabet.add(ele.lower())

                entity1 = re.findall("<e1>.*?</e1>", inline)#找到<e1>.*?</e1>
                entity2 = re.findall("<e2>.*?</e2>", inline)
                entity1 = re.sub("<e1>|</e1>", "", entity1[0])
                entity1 = re.sub("[^\w\d\s]", " ", entity1)

                entity2 = re.sub("<e2>|</e2>", "", entity2[0])
                entity2 = re.sub("[^\w\d\s]", " ", entity2)
                self.entity_alphabet.add(entity1.lower())
                self.entity_alphabet.add(entity2.lower())
                ent1_len = len(entity1.split())
                ent2_len = len(entity2.split())
                seq1 = re.sub("<e1>.*?</e1>", "#entity1#", inline)
                seq1 = re.sub("<e2>|</e2>", "", seq1)
                seq1 = re.sub(r'[^\w\d\s#]', ' ', seq1).split()
                ent1_pos = seq1.index("#entity1#")
                seq2 = re.sub("<e2>.*?</e2>", "#entity2#", inline)
                seq2 = re.sub("<e1>|</e1>", "", seq2)
                seq2 = re.sub(r'[^\w\d\s#]', ' ', seq2).split()
                ent2_pos = seq2.index("#entity2#")
                position1 = [] #计算每个词距离entity1的位置
                for j in range(len(seq1)):
                    if j != ent1_pos:
                        position1.append(j - ent1_pos)
                    else:
                        position1 += [0] * ent1_len #一个entity中的多个词位置都标记为0
                position2 = []#计算每个词距离entity2的位置
                for j in range(len(seq2)):
                    if j != ent2_pos:
                        position2.append(j - ent2_pos)
                    else:
                        position2 += [0] * ent2_len
                assert len(position1) == len(position2) == len(inline1)
                for k in range(len(position1)):
                    self.position_alphabet.add(position1[k])
                    self.position_alphabet.add(position2[k])
                label = line[4*i+1].rstrip()
                self.rel_label_alphabet.add(label)
        self.entity_alphabet_size = self.entity_alphabet.size()
        self.word_alphabet_size = self.word_alphabet.size()
        self.rel_label_alphabet_size = self.rel_label_alphabet.size()
        self.position_alphabet_size = self.position_alphabet.size()
        self.rel_label_alphabet.close()
        self.word_alphabet.close()
        self.position_alphabet.close()
        self.entity_alphabet.close()

    def load_pretrain_emb_dict(self, embedding_path):
        embed_dim = -1
        with open(embedding_path, 'r') as file:
            for line in tqdm(file):
                line = line.strip()
                if len(line) == 0:
                    continue
                tokens = line.split()
                length = len(tokens)
                if length <= 2:
                    continue
                if embed_dim < 0:
                    embed_dim = length - 1
                    self.word_embed_dim = embed_dim
                if embed_dim + 1 == length:
                    embed = np.empty([1, embed_dim])
                    embed[:] = tokens[1:]
                    self.embed_dict[tokens[0]] = embed
                    self.embed_alphabet[tokens[0]] = 1
        print("Finish building embed_alphabet")

    def build_pretrain_word_embedings(self):
        print("build word pretrain embeddings...")
        self.pretrain_word_embedding = build_pretrain_embedding(self.word_alphabet, self.embed_dict, self.word_embed_dim, norm=self.norm_emb)

    def build_pretrain_position_embeddings(self, posi_embed_dim):
        print("build position embedddings...")
        self.position_embed_dim = posi_embed_dim
        self.pretrain_position_embedding = None

    def generate_instance(self, input_file, name):
        """for NER"""
        ids = read_instance(input_file, self.word_alphabet, self.position_alphabet, self.rel_label_alphabet, self.entity_alphabet, self.MAX_SENTENCE_LENGTH)

        if name == "train":
            random.seed(1)
            random.shuffle(ids)#随机划分训练集与开发集
            self.train_ids = ids[:7200]
            self.dev_ids = ids[7200:]

        elif name == "test":
            self.test_ids = ids
        else:
            print("Error: you can only generate train/dev/test instance! Illegal input:%s" % (name))


