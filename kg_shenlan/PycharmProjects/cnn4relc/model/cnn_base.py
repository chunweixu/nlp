from __future__ import division
import numpy as np
import torch
import torch.nn as nn


class CNN_base(nn.Module):
    def __init__(self, data, args):
        super(CNN_base, self).__init__()
        # data.show_data_summary()
        self.embedding_dim = data.word_embed_dim
        self.pos_size = args.position_embed_dim
        self.word_embedding = nn.Embedding(data.word_alphabet.size(), self.embedding_dim, padding_idx=0)#输入pad的id，模型会自动不给pad梯度
        if data.pretrain_word_embedding is not None:
            self.word_embedding.weight.data.copy_(torch.from_numpy(data.pretrain_word_embedding))
        else:
            self.word_embedding.weight.data.copy_(
                torch.from_numpy(self.random_embedding(data.word_alphabet.size(), self.embedding_dim)))
        self.pos1_embedding = nn.Embedding(data.position_alphabet.size(), self.pos_size, padding_idx=0)#默认随时初始化模式
        self.pos2_embedding = nn.Embedding(data.position_alphabet.size(), self.pos_size, padding_idx=0)
        feature_dim = self.embedding_dim + self.pos_size * 2
        self.conv = nn.Conv2d(1, args.filters_num, (3, feature_dim), padding=(1, 0))#
        #nn.Conv2d(in_channels, out_channels, kernel_size, stride=1, padding=0,padding_mode='zeros')
        #pad（1,0）代表词的维度的句首和句尾各pad 一维，词向量维度不pad
        self.linear1 = nn.Linear(args.filters_num, args.hidden_layer_dim)#第一层线性变化
        self.linear2 = nn.Linear(args.hidden_layer_dim, data.rel_label_alphabet.size())
        self.init_model_weight()
        self.dropout = nn.Dropout(args.dropout)#声明一下dropout
        if args.use_gpu:
            self.word_embedding = self.word_embedding.cuda()
            self.pos1_embedding = self.pos1_embedding.cuda()
            self.pos2_embedding = self.pos2_embedding.cuda()
            self.conv = self.conv.cuda()
            self.linear1 = self.linear1.cuda()
            self.linear2 = self.linear2.cuda()
            self.dropout = self.dropout.cuda()

    def init_model_weight(self):
        nn.init.orthogonal_(self.pos1_embedding.weight.data)
        nn.init.orthogonal_(self.pos2_embedding.weight.data)#正交初始化
        nn.init.xavier_uniform_(self.conv.weight)#xavier平均初始化
        nn.init.constant_(self.conv.bias, 0.0)#常数初始化，这里用0做的
        nn.init.xavier_uniform_(self.linear1.weight)
        nn.init.constant_(self.linear1.bias, 0.0)
        nn.init.xavier_uniform_(self.linear2.weight)
        nn.init.constant_(self.linear2.bias, 0.0)

    def forward(self, word_tensor, posi1_tensor, posi2_tensor, mask):
        word_embedding = self.word_embedding(word_tensor)
        pf1 = self.pos1_embedding(posi1_tensor)
        pf2 = self.pos2_embedding(posi2_tensor)
        x = torch.cat([word_embedding, pf1, pf2], 2)
        # 将word embeddings和position embeddings拼接在一起
        x = self.dropout(x)
        x = x.unsqueeze(1)
        x = self.conv(x) #输入要求为4维（batchsize，input_channel,channel_height,channel_width）
        # 卷积操作
        x = x.squeeze(3)
        mask = mask.unsqueeze(1)
        x.masked_fill_(mask, -10**10)#把pad的地方替换成-10**10的值
        # 消除pad的影响，这一步可以删掉，不太影响结果
        x = nn.functional.max_pool1d(x, x.size()[2])
        # 最大池化操作
        x = x.squeeze(2)
        # 两层MLP
        x = torch.tanh(self.linear1(x))
        x = self.linear2(x)
        return x