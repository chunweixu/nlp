from utils.data import Data
from utils.batchify import batchify
from utils.evalution import marco_value
from model.cnn_base import CNN_base
import os
import re
import numpy as np
import argparse
import copy
import pickle
import torch
import torch.nn as nn
import torch.optim as optim
import time
import random
import sys
import gc

#初始化数据
def data_initialization(args):
    dataset_saved_file = args.dataset_saved_file
    data = Data()
    if os.path.exists(dataset_saved_file) and not args.refresh:
        data = load_data_setting(dataset_saved_file)
    else:
        data.norm_emb = args.norm_embedding
        #args.dataset_path=./data/SemEval/
        file_path = [args.dataset_path + ele for ele in os.listdir(args.dataset_path)]
        data.build_alphabet(file_path)
        data.load_pretrain_emb_dict(args.embedding_path)
        data.build_pretrain_word_embedings()#加载与训练embedding，如果没有的随机初始化
        data.build_pretrain_position_embeddings(args.position_embed_dim)#声明，在模型中随机初始化
        for ele in file_path:
            if re.findall("train", ele):
                data.generate_instance(ele, "train")
            else:
                data.generate_instance(ele, 'test')
        save_data_setting(data, dataset_saved_file)
    return data


def save_data_setting(data, dataset_saved_file):
    new_data = copy.deepcopy(data)
    if not os.path.exists(dataset_saved_file):
        os.makedirs(dataset_saved_file)
    dataset_saved_file = dataset_saved_file + "dataset.dset"
    with open(dataset_saved_file, 'wb') as fp:
        pickle.dump(new_data, fp)
    print("Data setting saved to file: ", dataset_saved_file)


def load_data_setting(dataset_saved_file):
    dataset_saved_file += "dataset.dset"
    with open(dataset_saved_file, 'rb') as fp:
        data = pickle.load(fp)
    print("Data setting loaded from file: ", dataset_saved_file)
    data.show_data_summary()
    return data


def lr_decay(optimizer, epoch, decay_rate, init_lr):
    lr = init_lr * ((1-decay_rate)**epoch)
    print(" Learning rate is setted as:", lr)
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr
    return optimizer

def train(data, args):
    print("Training model...")
    model = CNN_base(data, args)
    print("finished built model.")
    parameters = filter(lambda p: p.requires_grad, model.parameters())#获得所有需要优化的参数，不需要优化的参数在模型中提前定义
    optimizer = optim.Adam(parameters, lr=args.lr)
    criterion = nn.CrossEntropyLoss()
    best_dev = -1
    for idx in range(args.iteration):
        epoch_start = time.time()
        temp_start = epoch_start
        optimizer = lr_decay(optimizer, idx, args.lr_decay, args.lr)
        sample_loss = 0
        batch_loss = 0
        total_loss = 0
        random.shuffle(data.train_ids)
        model.train()
        model.zero_grad()
        batch_size = args.batch_size
        train_num = len(data.train_ids)
        total_batch = train_num//batch_size+1

        for batch_id in range(total_batch):
            start = batch_id*batch_size
            end = (batch_id+1)*batch_size
            if end > train_num:
                end = train_num
            instance = data.train_ids[start:end]
            if not instance:
                continue
            word_tensor, posi1_tensor, posi2_tensor, mask, label_tensor, seq_lens, ent1_tensor, ent2_tensor = batchify(instance, args.use_gpu)
            predict_distribution = model(word_tensor, posi1_tensor, posi2_tensor, mask)#等价于model.forward(word_tensor, posi1_tensor, posi2_tensor, mask)
            loss = criterion(predict_distribution, label_tensor)#一个batch中所有样本loss和
            sample_loss += loss.item()
            total_loss += loss.item()
            batch_loss += loss
            if end % 4000 == 0:
                temp_time = time.time()
                temp_cost = temp_time - temp_start
                temp_start = temp_time
                print("     Instance: %s; Time: %.2fs; loss: %.4f"%(end, temp_cost, sample_loss))
                sys.stdout.flush()
                sample_loss = 0
            if end % args.batch_size == 0:
                batch_loss.backward()
                optimizer.step()
                model.zero_grad()
                batch_loss = 0
        temp_time = time.time()
        temp_cost = temp_time - temp_start
        print("     Instance: %s; Time: %.2fs; loss: %.4f" % (end, temp_cost, sample_loss))
        epoch_finish = time.time()
        epoch_cost = epoch_finish - epoch_start
        print("Epoch: %s training finished. Time: %.2fs, speed: %.2fst/s,  total loss: %s" % (
        idx, epoch_cost, train_num / epoch_cost, total_loss))
        speed, p, r, f = evaluate(data, model, "dev", args)#在dev上评价一次
        dev_finish = time.time()
        dev_cost = dev_finish - epoch_finish
        print("Dev: time: %.2fs, speed: %.2fst/s; p: %.4f, r: %.4f, f: %.4f" % (dev_cost, speed, p, r, f))
        current_score = f
        if current_score > best_dev:
            print("Exceed previous best f score:", best_dev)
            model_name = args.model_saved_file + "epoch" + str(idx) + "_f1_" + str(
                current_score) + ".model"
            print("model save at: " + model_name)
            torch.save(model.state_dict(), model_name)
            best_dev = current_score
        gc.collect()


def evaluate(data, model, name, args):
    if name == "train":
        instances = data.train_ids
    elif name == "dev":
        instances = data.dev_ids
    elif name == "test":
        instances = data.test_ids
    else:
        print("Error: wrong evaluate name,", name)
    pred_results = []
    gold_results = []
    ## set model in eval model
    model.eval()
    batch_size = args.batch_size
    start_time = time.time()
    train_num = len(instances)
    total_batch = train_num//batch_size+1
    for batch_id in range(total_batch):
        start = batch_id*batch_size
        end = (batch_id+1)*batch_size
        if end > train_num:
            end = train_num
        instance = instances[start:end]
        if not instance:
            continue
        word_tensor, posi1_tensor, posi2_tensor, mask, label_tensor, seq_lens, ent1_tensor, ent2_tensor = batchify(instance, args.use_gpu)
        predict_distribution = model(word_tensor, posi1_tensor, posi2_tensor, mask)
        predict = torch.argmax(predict_distribution, dim=1).cpu()
        pred = predict.data.numpy().tolist()
        gold = label_tensor.cpu().data.numpy().tolist()
        pred_results = pred_results + pred
        gold_results = gold_results + gold
    decode_time = time.time() - start_time
    speed = len(instances)/decode_time
    p, r, f1 = marco_value(pred_results, gold_results, data.rel_label_alphabet.get_index("Other"), data.rel_label_alphabet_size)
    return speed, p, r, f1


def show_config(args):
    print("               -*- Model Config -*- ")
    print("                 Use_GPU: %s" % (args.use_gpu))
    print("               iteration: %s" % (args.iteration))
    print("              batch size: %s" % (args.batch_size))
    print("                      lr: %s" % (args.lr))
    print("                lr_decay: %s" % (args.lr_decay))
    print("          MLP hidden dim: %s" % (args.hidden_layer_dim))
    print("      position embed dim: %s" % (args.position_embed_dim))
    print("     3*3 conv filter num: %s" % (args.filters_num))
    print("                 dropout: %s" % (args.dropout))


def str2bool(v):
    return v.lower() in ('true')

if __name__ == '__main__':
    os.environ["CUDA_VISIBLE_DEVICES"] = "2"#显卡号
    parser = argparse.ArgumentParser(description='Tuning the model')#声明parser
    parser.add_argument('--status', choices=['train', 'test'], help='update algorithm', default='train')#选择模式
    parser.add_argument('--dataset_path', type=str, default='./data/SemEval/')#数据地址
    parser.add_argument('--embedding_path', type=str, default='./data/embedding/glove.6B.50d.txt')#词向量地址
    parser.add_argument('--dataset_saved_file', help='file of saved total data setting', default="./data/generate_data/")#保存的data类地址
    parser.add_argument('--model_saved_file', help='file to save model', default="./data/generate_data/")#保存的模型地址
    parser.add_argument('--refresh', help='refresh the file of saved data setting', default=False)#data是否重新加载
    parser.add_argument('--norm_embedding', help='norm word embedding', default=False)
    parser.add_argument('--iteration', default=100, type=int)
    parser.add_argument('--lr', default=0.001, type=float)
    parser.add_argument('--lr_decay', default=0.05, type=float)
    parser.add_argument('--batch_size', default=64, type=int)
    parser.add_argument('--filters_num', default=100, type=int)
    parser.add_argument('--hidden_layer_dim', default=200, type=int)#卷积结果，经过两次隐层变化得到最终的分类结果[卷积结果维度，隐层维度][隐层，关系类别]
    parser.add_argument('--position_embed_dim', default=12, type=int)#position embedding维度
    parser.add_argument('--dropout', default=0.3, type=float)
    parser.add_argument('--use_gpu', default=False, type=str2bool)
    seed = 1
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    np.random.seed(seed)
    random.seed(seed)
    torch.backends.cudnn.deterministic = True

    args = parser.parse_args()
    status = args.status.lower()
    show_config(args)
    if status == "train":
        data = data_initialization(args)
        train(data, args)



